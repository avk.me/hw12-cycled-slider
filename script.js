/* Ответ на теоретические вопросы:
1. setTimeout сработает один раз через период времени указанный в этой функции, а setInterval будет работать многократно периодически, каждый раз через указанный интервал времени.
2. Если в функцию setTimeout передать нулевую задержку, то браузер пропустит сначала эту функцию и выполнит все синхронные операции, а потом выполнятся все что передано в setTimeout.
3. Потому что иначе запущенный setInterval будем выполнятся без остановки.
*/

const stop = document.querySelector('.js-stop');
const cont = document.querySelector('.js-continue');
const pics = document.querySelectorAll('.image-to-show');
let count = 1;
let interval = "";

const showPicture = function () {
    pics.forEach(pic => {
        pic.classList.add('hidden-pic');
    })
    pics[count].classList.remove('hidden-pic');
    if (count === pics.length - 1) {
        count = -1;
    }
    count++;
}

stop.addEventListener('click', function () {
    clearInterval(interval);
    stop.disabled = true;
    cont.disabled = false;
})

cont.addEventListener('click', function () {
    interval = setInterval(showPicture, 3000);
    stop.disabled = false;
    cont.disabled = true;
})

interval = setInterval(showPicture, 3000);
